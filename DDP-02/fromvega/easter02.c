#include <stdio.h>
#include <stdlib.h>

#include "easter02_dictionary.h"
#include "easter02_graph.h"
#include "easter02_requests.h"

int main(int argc, char *argv[]) {
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <dictionary>\n", argv[0]);
		return EXIT_FAILURE;
	}
	if (!load_dictionary(argv[1])) {
		return EXIT_FAILURE;
	}
	if (!create_graph()) {
		return EXIT_FAILURE;
	}
	process_requests();
	return EXIT_SUCCESS;
}
