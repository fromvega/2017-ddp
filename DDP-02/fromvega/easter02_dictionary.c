#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define LETTERS_N_MAX 64

#include "easter02_types.h"
#include "easter02_common.h"
#include "easter02_dictionary.h"

word_t *add_word(unsigned long, char *);

unsigned long words_n;
word_t *words;

int load_dictionary(char *filename) {
char letters[LETTERS_N_MAX+2];
unsigned long letters_n;
FILE *fd;
	fd = fopen(filename, "r");
	if (!fd) {
		fprintf(stderr, "Could not open dictionary file %s\n", filename);
		return 0;
	}
	words_n = 0;
	while (fgets(letters, LETTERS_N_MAX+2, fd)) {
		letters_n = 0;
		for (letters_n = 0; letters[letters_n] && letters[letters_n] != '\n' && isupper((int)letters[letters_n]); letters_n++);
		if (!letters_n) {
			continue;
		}
		if (!letters[letters_n]) {
			fprintf(stderr, "Word %s... is too long\n", letters);
			free_words();
			fclose(fd);
			return 0;
		}
		if (letters[letters_n] != '\n') {
			fprintf(stderr, "Word %s contains non uppercase character(s)\n", letters);
			free_words();
			fclose(fd);
			return 0;
		}
		letters[letters_n] = '\0';
		if (!add_word(letters_n, letters)) {
			free_words();
			fclose(fd);
			return 0;
		}
	}
	fclose(fd);
	if (!words_n) {
		fprintf(stderr, "Dictionary contains no words\n");
		return 0;
	}
	return 1;
}

word_t *add_word(unsigned long letters_n, char *letters) {
word_t *words_tmp;
	if (words_n) {
		words_tmp = realloc(words, sizeof(word_t)*(words_n+1));
		if (!words_tmp) {
			fprintf(stderr, "Could not reallocate memory for words\n");
			return NULL;
		}
		words = words_tmp;
	}
	else {
		words = malloc(sizeof(word_t));
		if (!words) {
			fprintf(stderr, "Could not allocate memory for words\n");
			return NULL;
		}
	}
	if (!set_word(words+words_n, letters_n, letters)) {
		return NULL;
	}
	words_n++;
	return words;
}
