typedef struct {
	unsigned long letters_n;
	char *letters;
}
word_t;

typedef struct node_s node_t;
typedef struct link_s link_t;

struct node_s {
	word_t *word;
	node_t *root;
	unsigned long rank;
	unsigned long links_n;
	link_t *links;
	unsigned long froms_n;
	node_t **froms;
	int visited;
	unsigned long distance;
	node_t *last;
	node_t *next;
};

struct link_s {
	node_t *to;
	int visited;
};
