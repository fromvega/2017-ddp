#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define HASH_INIT 5381
#define HASH_SHIFT 5

#include "easter02_types.h"
#include "easter02_common.h"

extern unsigned long words_n, nodes_n, hash_size;
extern word_t *words;
extern node_t *nodes;

char *set_word(word_t *word, unsigned long letters_n, char *letters) {
	word->letters = malloc(letters_n+1);
	if (!word->letters) {
		fprintf(stderr, "Could not allocate memory for letters\n");
		return NULL;
	}
	word->letters_n = letters_n;
	strcpy(word->letters, letters);
	return word->letters;
}

node_t *get_node(word_t *word) {
unsigned long hash;
node_t *node;
	hash = get_hash(word);
	for (node = nodes[hash].next; node != nodes+hash && !compare_words(node->word, word); node = node->next);
	if (node == nodes+hash) {
		return NULL;
	}
	else {
		return node;
	}
}

unsigned long get_hash(word_t *word) {
unsigned long hash = HASH_INIT, i;
	for (i = 0; i < word->letters_n; i++) {
		hash += (hash << HASH_SHIFT)+(unsigned char)word->letters[i];
	}
	return hash%hash_size;
}

int compare_words(word_t *word1, word_t *word2) {
	return word1->letters_n == word2->letters_n && !strcmp(word1->letters, word2->letters);
}

node_t *find_root(node_t *node) {
	if (node->root != node) {
		node->root = find_root(node->root);
	}
	return node->root;
}

void free_nodes(void) {
unsigned long i;
	for (i = 0; i < nodes_n; i++) {
		if (nodes[i].links_n) {
			free(nodes[i].froms);
			free(nodes[i].links);
		}
	}
	free(nodes);
}

void free_words(void) {
unsigned long i;
	if (words_n) {
		for (i = 0; i < words_n; i++) {
			free(words[i].letters);
		}
		free(words);
	}
}
