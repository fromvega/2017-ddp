#include <stdio.h>
#include <stdlib.h>

#include "easter02_types.h"
#include "easter02_common.h"
#include "easter02_graph.h"

unsigned long next_prime(unsigned long);
int is_prime(unsigned long);
void link_node(node_t *, node_t *, node_t *);
int add_node(word_t *, node_t *);
void chain_node(node_t *, node_t *, node_t *);
int scan_letters(node_t *);
int scan_letter(node_t *, word_t *, char *, int, int);
link_t *add_link(node_t *, node_t *);
void set_link(link_t *, node_t *);
node_t **allocate_froms(node_t *);
void set_node(node_t *, word_t *);

extern unsigned long words_n;
extern word_t *words;

unsigned long nodes_n, hash_size, links_n;
node_t *nodes, **queue_bfs, **queue_dfs;

int create_graph(void) {
unsigned long hash, i;
node_t *node;
	nodes_n = next_prime(words_n);
	nodes = malloc(sizeof(node_t)*(nodes_n+words_n));
	if (!nodes) {
		fprintf(stderr, "Could not allocate memory for nodes\n");
		free_words();
		return 0;
	}
	for (i = 0; i < nodes_n; i++) {
		set_node(nodes+i, NULL);
		link_node(nodes+i, nodes+i, nodes+i);
	}
	hash_size = nodes_n;
	links_n = 0;
	for (i = 0; i < words_n; i++) {
		hash = get_hash(words+i);
		for (node = nodes[hash].next; node != nodes+hash && !compare_words(node->word, words+i); node = node->next);
		if (node == nodes+hash && !add_node(words+i, nodes+hash)) {
			free_nodes();
			free_words();
			return 0;
		}
	}
	for (i = hash_size; i < nodes_n; i++) {
		if (!allocate_froms(nodes+i)) {
			free_nodes();
			free_words();
			return 0;
		}
	}
	if (nodes_n-hash_size < links_n) {
		queue_bfs = malloc(sizeof(node_t *)*links_n);
	}
	else {
		queue_bfs = malloc(sizeof(node_t *)*(nodes_n-hash_size));
	}
	if (!queue_bfs) {
		fprintf(stderr, "Could not allocate memory for queue_bfs\n");
		free_nodes();
		free_words();
		return 0;
	}
	queue_dfs = malloc(sizeof(node_t *)*(nodes_n-hash_size));
	if (!queue_dfs) {
		fprintf(stderr, "Could not allocate memory for queue_dfs\n");
		free(queue_bfs);
		free_nodes();
		free_words();
		return 0;
	}
	return 1;
}

unsigned long next_prime(unsigned long val) {
	val |= 1;
	while (!is_prime(val)) {
		val += 2;
	}
	return val;
}

int is_prime(unsigned long val) {
unsigned long i;
	for (i = 3; i*i <= val && val%i; i += 2);
	return i*i > val;
}

void link_node(node_t *node, node_t *last, node_t *next) {
	node->last = last;
	node->next = next;
}

int add_node(word_t *word, node_t *node_hash) {
	set_node(nodes+nodes_n, word);
	chain_node(nodes+nodes_n, node_hash->last, node_hash);
	if (!scan_letters(nodes+nodes_n)) {
		return 0;
	}
	nodes_n++;
	return 1;
}

void chain_node(node_t *node, node_t *last, node_t *next) {
	node->last = last;
	last->next = node;
	node->next = next;
	next->last = node;
}

int scan_letters(node_t *node) {
unsigned long i;
word_t word;
	if (!set_word(&word, node->word->letters_n, node->word->letters)) {
		return 0;
	}
	for (i = 0; i < word.letters_n; i++) {
		if (!scan_letter(node, &word, word.letters+i, 'A', word.letters[i]-1) || !scan_letter(node, &word, word.letters+i, word.letters[i]+1, 'Z')) {
			free(word.letters);
			return 0;
		}
	}
	free(word.letters);
	return 1;
}

int scan_letter(node_t *node, word_t *word, char *letter, int letter_min, int letter_max) {
char letter_orig;
node_t *to, *node_root, *to_root;
	letter_orig = *letter;
	for (*letter = (char)letter_min; *letter <= (char)letter_max; *letter = (char)(*letter+1)) {
		to = get_node(word);
		if (to) {
			if (!add_link(node, to) || !add_link(to, node)) {
				return 0;
			}
			node_root = find_root(node);
			to_root = find_root(to);
			if (node_root != to_root) {
				if (node_root->rank < to_root->rank) {
					node_root->root = to_root;
				}
				else if (node_root->rank > to_root->rank) {
					to_root->root = node_root;
				}
				else {
					node_root->rank++;
					to_root->root = node_root;
				}
			}
			links_n++;
		}
	}
	*letter = letter_orig;
	return 1;
}

link_t *add_link(node_t *from, node_t *to) {
link_t *links_tmp;
	if (from->links_n) {
		links_tmp = realloc(from->links, sizeof(link_t)*(from->links_n+1));
		if (!links_tmp) {
			fprintf(stderr, "Could not reallocate memory for links\n");
			return NULL;
		}
		from->links = links_tmp;
	}
	else {
		from->links = malloc(sizeof(link_t));
		if (!from->links) {
			fprintf(stderr, "Could not allocate memory for links\n");
			return NULL;
		}
	}
	set_link(from->links+from->links_n, to);
	from->links_n++;
	return from->links;
}

void set_link(link_t *link, node_t *to) {
	link->to = to;
	link->visited = 0;
}

node_t **allocate_froms(node_t *node) {
	node->froms = malloc(sizeof(node_t *)*node->links_n);
	if (!node->froms) {
		fprintf(stderr, "Could not allocate memory for node->froms\n");
	}
	return node->froms;
}

void set_node(node_t *node, word_t *word) {
	node->word = word;
	node->root = node;
	node->rank = 0;
	node->links_n = 0;
	node->froms_n = 0;
	node->visited = 0;
}
