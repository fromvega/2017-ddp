char *set_word(word_t *, unsigned long, char *);
node_t *get_node(word_t *);
unsigned long get_hash(word_t *);
int compare_words(word_t *, word_t *);
node_t *find_root(node_t *);
void free_nodes(void);
void free_words(void);
