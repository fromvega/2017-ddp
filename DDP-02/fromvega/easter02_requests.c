#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define REQ_LEN_MAX 256
#define REQ_NEIGHBOURS "n"
#define REQ_NEIGHBOURS_DOT "nd"
#define REQ_MOST_NEIGHBOURS "nm"
#define REQ_MOST_NEIGHBOURS_DOT "nmd"
#define REQ_RANDOM_PATH "r"
#define REQ_RANDOM_PATH_DOT "rd"
#define REQ_SHORTEST_PATH "s"
#define REQ_SHORTEST_PATH_DOT "sd"
#define REQ_ALL_SHORTEST_PATHS "sa"
#define REQ_ALL_SHORTEST_PATHS_DOT "sad"
#define REQ_LONGEST_PATH "l"
#define REQ_LONGEST_PATH_DOT "ld"
#define REQ_HELP "?"
#define REQ_QUIT "q"

#include "easter02_types.h"
#include "easter02_common.h"
#include "easter02_requests.h"

void neighbours(char *, int);
void most_neighbours(int);
void random_path(char *, char *, int);
void test_node_random_bfs(node_t *);
int search_random_path(node_t *, node_t *, unsigned long);
void shortest_path(char *, char *, int);
void test_node_shortest(node_t *, node_t *);
void all_shortest_paths(char *, char *, int);
void test_link(node_t *, link_t *);
void print_steps_dot(node_t *);
void print_steps(node_t *, unsigned long, unsigned long);
void longest_path(char *, char *, int);
void test_node_longest_bfs(node_t *, node_t *);
int sort_links(const void *, const void *);
void search_longest_path(node_t *, node_t *, unsigned long);
int is_valid_path(char *, node_t **, char *, node_t **);
void add_to_queue_bfs(node_t *);
void test_node_longest_dfs(node_t *);
void add_to_queue_dfs(node_t *);
void print_path(node_t **, unsigned long);
void print_link(node_t *, node_t *, int);

extern unsigned long nodes_n, hash_size;
extern node_t *nodes, **queue_bfs, **queue_dfs;

unsigned long queue_bfs_size, steps_n_max, queue_dfs_size;
node_t **steps, **steps_max;

void process_requests(void) {
char req[REQ_LEN_MAX+2], *req_type, *req_word1, *req_word2, *req_end;
unsigned long req_len;
	do {
		printf("REQ> ");
		fflush(stdout);
		if (!fgets(req, REQ_LEN_MAX+2, stdin)) {
			break;
		}
		req_len = strlen(req);
		if (req[req_len-1] == '\0') {
			fprintf(stderr, "Request %s... is too long\n", req);
			continue;
		}
		req[req_len-1] = '\0';
		req_len--;
		if (!req_len) {
			continue;
		}
		req_type = strtok(req, " ");
		req_word1 = strtok(NULL, " ");
		req_word2 = strtok(NULL, " ");
		req_end = strtok(NULL, "\n");
		if (!strcmp(req_type, REQ_NEIGHBOURS)) {
			if (!req_word1 || req_word2 || req_end) {
				fprintf(stderr, "Usage: %s <word> - neighbours of <word>\n", req_type);
				continue;
			}
			neighbours(req_word1, 0);
		}
		else if (!strcmp(req_type, REQ_NEIGHBOURS_DOT)) {
			if (!req_word1 || req_word2 || req_end) {
				fprintf(stderr, "Usage: %s <word> - neighbours of <word> - dot format\n", req_type);
				continue;
			}
			neighbours(req_word1, 1);
		}
		else if (!strcmp(req_type, REQ_MOST_NEIGHBOURS)) {
			if (req_word1 || req_word2 || req_end) {
				fprintf(stderr, "Usage: %s - word with the most neighbours\n", req_type);
				continue;
			}
			most_neighbours(0);
		}
		else if (!strcmp(req_type, REQ_MOST_NEIGHBOURS_DOT)) {
			if (req_word1 || req_word2 || req_end) {
				fprintf(stderr, "Usage: %s - word with the most neighbours - dot format\n", req_type);
				continue;
			}
			most_neighbours(1);
		}
		else if (!strcmp(req_type, REQ_RANDOM_PATH)) {
			if (!req_word1 || !req_word2 || req_end) {
				fprintf(stderr, "Usage: %s <word1> <word2> - random path from <word1> to <word2>\n", req_type);
				continue;
			}
			random_path(req_word1, req_word2, 0);
		}
		else if (!strcmp(req_type, REQ_RANDOM_PATH_DOT)) {
			if (!req_word1 || !req_word2 || req_end) {
				fprintf(stderr, "Usage: %s <word1> <word2> - random path from <word1> to <word2> - dot format\n", req_type);
				continue;
			}
			random_path(req_word1, req_word2, 1);
		}
		else if (!strcmp(req_type, REQ_SHORTEST_PATH)) {
			if (!req_word1 || !req_word2 || req_end) {
				fprintf(stderr, "Usage: %s <word1> <word2> - one shortest path from <word1> to <word2>\n", req_type);
				continue;
			}
			shortest_path(req_word1, req_word2, 0);
		}
		else if (!strcmp(req_type, REQ_SHORTEST_PATH_DOT)) {
			if (!req_word1 || !req_word2 || req_end) {
				fprintf(stderr, "Usage: %s <word1> <word2> - one shortest path from <word1> to <word2> - dot format\n", req_type);
				continue;
			}
			shortest_path(req_word1, req_word2, 1);
		}
		else if (!strcmp(req_type, REQ_ALL_SHORTEST_PATHS)) {
			if (!req_word1 || !req_word2 || req_end) {
				fprintf(stderr, "Usage: %s <word1> <word2> - all shortest paths from <word1> to <word2>\n", req_type);
				continue;
			}
			all_shortest_paths(req_word1, req_word2, 0);
		}
		else if (!strcmp(req_type, REQ_ALL_SHORTEST_PATHS_DOT)) {
			if (!req_word1 || !req_word2 || req_end) {
				fprintf(stderr, "Usage: %s <word1> <word2> - all shortest paths from <word1> to <word2> - dot format\n", req_type);
				continue;
			}
			all_shortest_paths(req_word1, req_word2, 1);
		}
		else if (!strcmp(req_type, REQ_LONGEST_PATH)) {
			if (!req_word1 || !req_word2 || req_end) {
				fprintf(stderr, "Usage: %s <word1> <word2> - one longest path from <word1> to <word2>\n", req_type);
				continue;
			}
			longest_path(req_word1, req_word2, 0);
		}
		else if (!strcmp(req_type, REQ_LONGEST_PATH_DOT)) {
			if (!req_word1 || !req_word2 || req_end) {
				fprintf(stderr, "Usage: %s <word1> <word2> - one longest path from <word1> to <word2> - dot format\n", req_type);
				continue;
			}
			longest_path(req_word1, req_word2, 1);
		}
		else if (!strcmp(req_type, REQ_HELP)) {
			if (req_word1 || req_word2 || req_end) {
				fprintf(stderr, "Usage: %s - help\n", req_type);
				continue;
			}
			puts("Available requests");
			printf("%s <word> - neighbours of <word>\n", REQ_NEIGHBOURS);
			printf("%s <word> - neighbours of <word> - dot format\n", REQ_NEIGHBOURS_DOT);
			printf("%s - word with most neighbours\n", REQ_MOST_NEIGHBOURS);
			printf("%s - word with most neighbours - dot format\n", REQ_MOST_NEIGHBOURS_DOT);
			printf("%s <word1> <word2> - random path from <word1> to <word2>\n", REQ_RANDOM_PATH);
			printf("%s <word1> <word2> - random path from <word1> to <word2> - dot format\n", REQ_RANDOM_PATH_DOT);
			printf("%s <word1> <word2> - one shortest path from <word1> to <word2>\n", REQ_SHORTEST_PATH);
			printf("%s <word1> <word2> - one shortest path from <word1> to <word2> - dot format\n", REQ_SHORTEST_PATH_DOT);
			printf("%s <word1> <word2> - all shortest paths from <word1> to <word2>\n", REQ_ALL_SHORTEST_PATHS);
			printf("%s <word1> <word2> - all shortest paths from <word1> to <word2> - dot format\n", REQ_ALL_SHORTEST_PATHS_DOT);
			printf("%s <word1> <word2> - one longest path from <word1> to <word2>\n", REQ_LONGEST_PATH);
			printf("%s <word1> <word2> - one longest path from <word1> to <word2> - dot format\n", REQ_LONGEST_PATH_DOT);
			printf("%s - help\n", REQ_HELP);
			printf("%s - quit\n", REQ_QUIT);
		}
		else if (!strcmp(req_type, REQ_QUIT)) {
			if (req_word1 || req_word2 || req_end) {
				fprintf(stderr, "Usage: %s (quit)\n", req_type);
				continue;
			}
			break;
		}
		else {
			fprintf(stderr, "Request %s is not available\n", req_type);
		}
	}
	while (1);
	free(queue_dfs);
	free(queue_bfs);
	free_nodes();
	free_words();
}

void neighbours(char *letters, int dot) {
unsigned long i;
word_t word;
node_t *node;
	if (!set_word(&word, strlen(letters), letters)) {
		return;
	}
	node = get_node(&word);
	if (!node) {
		fprintf(stderr, "Word %s does not exist\n", letters);
		free(word.letters);
		return;
	}
	if (dot) {
		printf("graph neighbours_%s {\n", word.letters);
		printf("\t# %lu\n", node->links_n);
	}
	else {
		printf("Neighbours of %s (%lu)\n", word.letters, node->links_n);
	}
	for (i = 0; i < node->links_n; i++) {
		print_link(node, node->links[i].to, dot);
	}
	if (dot) {
		puts("}");
	}
	free(word.letters);
}

void most_neighbours(int dot) {
unsigned long i;
node_t *node_most = nodes+hash_size;
	for (i = hash_size+1; i < nodes_n; i++) {
		if (nodes[i].links_n > node_most->links_n) {
			node_most = nodes+i;
		}
	}
	if (dot) {
		puts("graph most_neighbours {");
		printf("\t# %lu\n", node_most->links_n);
	}
	else {
		printf("Word with most neighbours is %s (%lu)\n", node_most->word->letters, node_most->links_n);
	}
	for (i = 0; i < node_most->links_n; i++) {
		print_link(node_most, node_most->links[i].to, dot);
	}
	if (dot) {
		puts("}");
	}
}

void random_path(char *letters1, char *letters2, int dot) {
unsigned long i, j;
node_t *node1, *node2;
	if (!is_valid_path(letters1, &node1, letters2, &node2)) {
		return;
	}
	node2->visited = 1;
	queue_bfs[0] = node2;
	queue_bfs_size = 1;
	for (i = 0; i < queue_bfs_size; i++) {
		for (j = 0; j < queue_bfs[i]->links_n; j++) {
			test_node_random_bfs(queue_bfs[i]->links[j].to);
		}
	}
	for (i = 0; i < queue_bfs_size; i++) {
		queue_bfs[i]->visited = 0;
	}
	steps = malloc(sizeof(node_t *)*queue_bfs_size);
	if (steps) {
		steps_n_max = 0;
		search_random_path(node1, node2, 0UL);
		if (dot) {
			printf("graph random_path_%s_%s {\n", letters1, letters2);
			printf("\t# Length %lu\n", steps_n_max);
			if (steps_n_max) {
				for (i = 0; i < steps_n_max; i++) {
					print_link(steps[i], steps[i+1], dot);
				}
			}
			else {
				print_link(steps[0], steps[0], dot);
			}
			puts("}");
		}
		else {
			printf("Random path from %s to %s (length %lu)\n", letters1, letters2, steps_n_max);
			print_path(steps, steps_n_max);
		}
		free(steps);
	}
	else {
		fprintf(stderr, "Could not allocate memory for steps\n");
	}
}

void test_node_random_bfs(node_t *to) {
	if (!to->visited) {
		to->visited = 1;
		add_to_queue_bfs(to);
	}
}

int search_random_path(node_t *node, node_t *to, unsigned long steps_n) {
int path_found;
unsigned long i, j;
	steps[steps_n] = node;
	if (node == to) {
		if (steps_n > steps_n_max) {
			steps_n_max = steps_n;
		}
		return 1;
	}
	else {
		node->visited = 1;
		queue_dfs[0] = node;
		queue_dfs_size = 1;
		for (i = 0; i < queue_dfs_size && queue_dfs[i] != to; i++) {
			for (j = 0; j < queue_dfs[i]->links_n; j++) {
				test_node_longest_dfs(queue_dfs[i]->links[j].to);
			}
		}
		path_found = i < queue_dfs_size;
		for (i = 1; i < queue_dfs_size; i++) {
			queue_dfs[i]->visited = 0;
		}
		if (path_found) {
			path_found = 0;
			for (i = 0; i < node->links_n && !path_found; i++) {
				if (!node->links[i].to->visited) {
					path_found = search_random_path(node->links[i].to, to, steps_n+1);
				}
			}
		}
		node->visited = 0;
		return path_found;
	}
}

void shortest_path(char *letters1, char *letters2, int dot) {
unsigned long i, j;
node_t *node1, *node2;
	if (!is_valid_path(letters1, &node1, letters2, &node2)) {
		return;
	}
	node1->visited = 1;
	node1->distance = 0;
	queue_bfs[0] = node1;
	queue_bfs_size = 1;
	for (i = 0; i < queue_bfs_size && queue_bfs[i] != node2; i++) {
		for (j = 0; j < queue_bfs[i]->links_n; j++) {
			test_node_shortest(queue_bfs[i], queue_bfs[i]->links[j].to);
		}
	}
	if (dot) {
		printf("graph one_shortest_path_%s_%s {\n", letters1, letters2);
		printf("\t# Length %lu\n", node2->distance);
		if (node1 == node2) {
			print_link(node1, node2, dot);
		}
		else {
			print_steps_dot(node2);
		}
		puts("}");
	}
	else {
		if (node1 == node2) {
			printf("One shortest path from %s to %s (length %lu)\n", letters1, letters2, node2->distance);
			puts(node1->word->letters);
		}
		else {
			steps = malloc(sizeof(node_t *)*(node2->distance+1));
			if (steps) {
				printf("One shortest path from %s to %s (length %lu)\n", letters1, letters2, node2->distance);
				print_steps(node2, node2->distance, node2->distance);
				free(steps);
			}
			else {
				fprintf(stderr, "Could not allocate memory for steps\n");
			}
		}
	}
	for (i = 0; i < queue_bfs_size; i++) {
		queue_bfs[i]->froms_n = 0;
		queue_bfs[i]->visited = 0;
		queue_bfs[i]->distance = 0;
	}
}

void test_node_shortest(node_t *from, node_t *to) {
	if (!to->visited) {
		to->froms[to->froms_n++] = from;
		to->visited = 1;
		to->distance = from->distance+1;
		add_to_queue_bfs(to);
	}
}

void all_shortest_paths(char *letters1, char *letters2, int dot) {
int path_found;
unsigned long i, j;
node_t *node1, *node2;
	if (!is_valid_path(letters1, &node1, letters2, &node2)) {
		return;
	}
	node1->visited = 1;
	node1->distance = 0;
	queue_bfs[0] = node1;
	queue_bfs_size = 1;
	path_found = 0;
	for (i = 0; i < queue_bfs_size && (!path_found || queue_bfs[i]->distance <= node2->distance); i++) {
		if (queue_bfs[i] == node2) {
			path_found = 1;
		}
		for (j = 0; j < queue_bfs[i]->links_n; j++) {
			test_link(queue_bfs[i], queue_bfs[i]->links+j);
		}
	}
	if (dot) {
		printf("graph all_shortest_paths_%s_%s {\n", letters1, letters2);
		printf("\t# Length %lu\n", node2->distance);
		if (node1 == node2) {
			print_link(node1, node2, dot);
		}
		else {
			print_steps_dot(node2);
		}
		puts("}");
	}
	else {
		if (node1 == node2) {
			printf("All shortest paths from %s to %s (length %lu)\n", letters1, letters2, node2->distance);
			puts(node1->word->letters);
		}
		else {
			steps = malloc(sizeof(node_t *)*(node2->distance+1));
			if (steps) {
				printf("All shortest paths from %s to %s (length %lu)\n", letters1, letters2, node2->distance);
				print_steps(node2, node2->distance, node2->distance);
				free(steps);
			}
			else {
				fprintf(stderr, "Could not allocate memory for path\n");
			}
		}
	}
	for (i = 0; i < queue_bfs_size; i++) {
		for (j = 0; j < queue_bfs[i]->links_n; j++) {
			if (queue_bfs[i]->links[j].visited) {
				queue_bfs[i]->links[j].visited = 0;
			}
		}
		queue_bfs[i]->froms_n = 0;
		queue_bfs[i]->visited = 0;
		queue_bfs[i]->distance = 0;
	}
}

void test_link(node_t *from, link_t *link) {
	if ((!link->to->visited || link->to->distance == from->distance+1) && !link->visited) {
		link->to->froms[link->to->froms_n++] = from;
		if (!link->to->visited) {
			link->to->visited = 1;
			link->to->distance = from->distance+1;
		}
		link->visited = 1;
		add_to_queue_bfs(link->to);
	}
}

void print_steps_dot(node_t *node) {
unsigned long i;
	if (node->froms_n && node->visited < 2) {
		node->visited = 2;
		for (i = 0; i < node->froms_n; i++) {
			print_steps_dot(node->froms[i]);
			print_link(node->froms[i], node, 1);
		}
	}
}

void print_steps(node_t *node, unsigned long step_idx, unsigned long steps_n) {
unsigned long i;
	steps[step_idx] = node;
	if (node->froms_n) {
		for (i = 0; i < node->froms_n; i++) {
			print_steps(node->froms[i], step_idx-1, steps_n);
		}
	}
	else {
		print_path(steps, steps_n);
	}
}

void longest_path(char *letters1, char *letters2, int dot) {
unsigned long i, j;
node_t *node1, *node2;
	if (!is_valid_path(letters1, &node1, letters2, &node2)) {
		return;
	}
	node2->visited = 1;
	node2->distance = 0;
	queue_bfs[0] = node2;
	queue_bfs_size = 1;
	for (i = 0; i < queue_bfs_size; i++) {
		for (j = 0; j < queue_bfs[i]->links_n; j++) {
			test_node_longest_bfs(queue_bfs[i], queue_bfs[i]->links[j].to);
		}
	}
	for (i = 0; i < queue_bfs_size; i++) {
		qsort(queue_bfs[i]->links, queue_bfs[i]->links_n, sizeof(link_t), sort_links);
		queue_bfs[i]->visited = 0;
	}
	steps = malloc(sizeof(node_t *)*queue_bfs_size);
	if (steps) {
		steps_max = malloc(sizeof(node_t *)*queue_bfs_size);
		if (steps_max) {
			steps_max[0] = node1;
			steps_n_max = 0;
			search_longest_path(node1, node2, 0UL);
			if (dot) {
				printf("graph one_longest_path_%s_%s {\n", letters1, letters2);
				printf("\t# Length %lu\n", steps_n_max);
				if (steps_n_max) {
					for (i = 0; i < steps_n_max; i++) {
						print_link(steps_max[i], steps_max[i+1], dot);
					}
				}
				else {
					print_link(steps_max[0], steps_max[0], dot);
				}
				puts("}");
			}
			else {
				printf("One longest path from %s to %s (length %lu)\n", letters1, letters2, steps_n_max);
				print_path(steps_max, steps_n_max);
			}
			free(steps_max);
		}
		else {
			fprintf(stderr, "Could not allocate memory for steps_max\n");
		}
		free(steps);
	}
	else {
		fprintf(stderr, "Could not allocate memory for steps\n");
	}
	for (i = 0; i < queue_bfs_size; i++) {
		queue_bfs[i]->distance = 0;
	}
}

void test_node_longest_bfs(node_t *from, node_t *to) {
	if (!to->visited) {
		to->visited = 1;
		to->distance = from->distance+1;
		add_to_queue_bfs(to);
	}
}

int sort_links(const void *a, const void *b) {
const link_t *link_a = (const link_t *)a, *link_b = (const link_t *)b;
	if (link_a->to->distance > link_b->to->distance) {
		return -1;
	}
	else if (link_a->to->distance < link_b->to->distance) {
		return 1;
	}
	else {
		return 0;
	}
}

void search_longest_path(node_t *node, node_t *to, unsigned long steps_n) {
int path_found;
unsigned long i, j;
	steps[steps_n] = node;
	if (node == to) {
		if (steps_n > steps_n_max) {
			for (i = 1; i <= steps_n; i++) {
				steps_max[i] = steps[i];
			}
			steps_n_max = steps_n;
			printf("# New maximal length found %lu\n", steps_n_max);
		}
	}
	else {
		node->visited = 1;
		queue_dfs[0] = node;
		queue_dfs_size = 1;
		path_found = 0;
		for (i = 0; i < queue_dfs_size; i++) {
			if (queue_dfs[i] == to) {
				path_found = 1;
			}
			for (j = 0; j < queue_dfs[i]->links_n; j++) {
				test_node_longest_dfs(queue_dfs[i]->links[j].to);
			}
		}
		for (i = 1; i < queue_dfs_size; i++) {
			queue_dfs[i]->visited = 0;
		}
		if ((!steps_n_max || queue_dfs_size-1 > steps_n_max-steps_n) && path_found) {
			for (i = 0; i < node->links_n; i++) {
				if (!node->links[i].to->visited) {
					search_longest_path(node->links[i].to, to, steps_n+1);
				}
			}
		}
		node->visited = 0;
	}
}

int is_valid_path(char *letters1, node_t **node1, char *letters2, node_t **node2) {
word_t word1, word2;
node_t *root1, *root2;
	if (!set_word(&word1, strlen(letters1), letters1)) {
		return 0;
	}
	*node1 = get_node(&word1);
	if (*node1 == NULL) {
		fprintf(stderr, "Word %s does not exist\n", letters1);
		free(word1.letters);
		return 0;
	}
	if (!set_word(&word2, strlen(letters2), letters2)) {
		free(word1.letters);
		return 0;
	}
	*node2 = get_node(&word2);
	if (*node2 == NULL) {
		fprintf(stderr, "Word %s does not exist\n", letters2);
		free(word2.letters);
		free(word1.letters);
		return 0;
	}
	root1 = find_root(*node1);
	root2 = find_root(*node2);
	if (root1 != root2) {
		fprintf(stderr, "No path from %s to %s\n", letters1, letters2);
		free(word2.letters);
		free(word1.letters);
		return 0;
	}
	free(word2.letters);
	free(word1.letters);
	return 1;
}

void add_to_queue_bfs(node_t *node) {
	queue_bfs[queue_bfs_size++] = node;
}

void test_node_longest_dfs(node_t *to) {
	if (!to->visited) {
		to->visited = 1;
		add_to_queue_dfs(to);
	}
}

void add_to_queue_dfs(node_t *node) {
	queue_dfs[queue_dfs_size++] = node;
}

void print_path(node_t **path, unsigned long path_size) {
unsigned long i;
	printf("%s", path[0]->word->letters);
	for (i = 1; i <= path_size; i++) {
		printf(" -- %s", path[i]->word->letters);
	}
	puts("");
}

void print_link(node_t *from, node_t *to, int dot) {
	if (dot) {
		printf("\t%s", from->word->letters);
		if (from != to) {
			printf(" -- %s", to->word->letters);
		}
		puts(";");
	}
	else {
		puts(to->word->letters);
	}
}
