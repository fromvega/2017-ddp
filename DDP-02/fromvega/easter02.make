EASTER02_C_FLAGS=-c -O2 -Wall -Wextra -Waggregate-return -Wcast-align -Wcast-qual -Wconversion -Wformat=2 -Winline -Wlong-long -Wmissing-prototypes -Wmissing-declarations -Wnested-externs -Wno-import -Wpointer-arith -Wredundant-decls -Wreturn-type -Wshadow -Wstrict-prototypes -Wswitch -Wwrite-strings
EASTER02_OBJS=easter02_common.o easter02_dictionary.o easter02_graph.o easter02_requests.o easter02.o

easter02: ${EASTER02_OBJS}
	gcc -o easter02 ${EASTER02_OBJS}

easter02_common.o: easter02_common.c easter02_types.h easter02_common.h easter02.make
	gcc ${EASTER02_C_FLAGS} -o easter02_common.o easter02_common.c

easter02_dictionary.o: easter02_dictionary.c easter02_types.h easter02_common.h easter02_dictionary.h easter02.make
	gcc ${EASTER02_C_FLAGS} -o easter02_dictionary.o easter02_dictionary.c

easter02_graph.o: easter02_graph.c easter02_types.h easter02_common.h easter02_graph.h easter02.make
	gcc ${EASTER02_C_FLAGS} -o easter02_graph.o easter02_graph.c

easter02_requests.o: easter02_requests.c easter02_types.h easter02_common.h easter02_requests.h easter02.make
	gcc ${EASTER02_C_FLAGS} -o easter02_requests.o easter02_requests.c

easter02.o: easter02.c easter02_dictionary.h easter02_graph.h easter02_requests.h easter02.make
	gcc ${EASTER02_C_FLAGS} -o easter02.o easter02.c

clean:
	rm -f easter02 ${EASTER02_OBJS}
